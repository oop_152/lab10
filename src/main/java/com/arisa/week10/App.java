package com.arisa.week10;


public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec = new Rectangle(5,3);
        System.out.println(rec.toString());
        System.out.printf("%s area: %.2f \n",rec.getName(),rec.calArea());
        System.out.printf("%s perimeter: %.2f \n",rec.getName(),rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2,2);
        System.out.println(rec2.toString());
        System.out.printf("%s area: %.2f \n",rec2.getName(),rec2.calArea());
        System.out.printf("%s perimeter: %.2f \n",rec2.getName(),rec2.calPerimeter());

        Circle circle =  new Circle(2);
        System.out.printf("%s area: %.3f \n",circle.getName(),circle.calArea());
        System.out.printf("%s perimeter: %.3f \n",circle.getName(),circle.calPerimeter());

        Circle circle2 =  new Circle(3);
        System.out.printf("%s area: %.3f \n",circle2.getName(),circle.calArea());
        System.out.printf("%s perimeter: %.3f \n",circle2.getName(),circle.calPerimeter());

        Triangle tri = new Triangle(2, 2, 2);
        System.out.printf("%s area: %.2f \n",tri.getName(),tri.calArea());
        System.out.printf("%s perimeter: %.2f \n",tri.getName(),tri.calPerimeter());
    }
}
